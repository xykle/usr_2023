using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anchor : MonoBehaviour
{
    public const float delayTime = 1.0f;

    private void OnEnable()
    {
        StartCoroutine(DelayTrack());
    }

    private IEnumerator DelayTrack()
    {
        yield return new WaitForSeconds(delayTime);
        GameManager.Instance.OnARImageTracked(transform);
    }
}
