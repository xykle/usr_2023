using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.DualShock;

public class GameManager : MonoBehaviour
{
    // 單例化
    #region Singalton
    public static GameManager Instance;

    private void InitializeSingalton()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    public bool isObjectActived { get; private set; } = false;

    [SerializeField] private GameObject testModel;
    [SerializeField] private float duration;

    private void Awake()
    {
        // 呼叫單利化方法
        InitializeSingalton();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void OnARImageTracked(Transform objTransform)
    {
        if (isObjectActived)
            return;

        testModel.transform.position = objTransform.position;
        testModel.transform.rotation = objTransform.rotation;

        StartCoroutine(DissolveEffect());
        testModel.SetActive(true);
        isObjectActived = true;
    }

    public void OnARImageDistrack()
    {
        testModel.SetActive(false);
        isObjectActived = false;
    }

    private IEnumerator DissolveEffect()
    {
        var timer = 0f;
        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        testModel.transform.GetChild(0).GetComponent<Renderer>().GetPropertyBlock(mpb);

        while (timer < duration)
        {
            mpb.SetFloat("_DissolveAmount", Mathf.Lerp(1f, 0f, timer / duration));
            testModel.transform.GetChild(0).GetComponent<Renderer>().SetPropertyBlock(mpb);

            yield return null;
            timer += Time.deltaTime;
        }
        mpb.SetFloat("_DissolveAmount", 0f);
        testModel.transform.GetChild(0).GetComponent<Renderer>().SetPropertyBlock(mpb);
    }
}

public enum GameLocation
{
    None = 0,
    BeiShiMarina = 1
}
